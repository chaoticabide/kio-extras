# translation of kio_nfs.po to Kazakh
# Sairan Kikkarin <sairan@computer.org>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 02:01+0000\n"
"PO-Revision-Date: 2005-10-25 16:46+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"\n"

#: kio_nfs.cpp:153
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr ""

#: kio_nfs.cpp:323
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr ""

#: kio_nfs.cpp:353
#, kde-format
msgid "Failed to initialise protocol"
msgstr ""

#: kio_nfs.cpp:821
#, kde-format
msgid "RPC error %1, %2"
msgstr ""

#: kio_nfs.cpp:870
#, kde-format
msgid "Filename too long"
msgstr "Файл атауы тым ұзым"

#: kio_nfs.cpp:877
#, kde-format
msgid "Disk quota exceeded"
msgstr "Дискідегі квота бітті"

#: kio_nfs.cpp:883
#, kde-format
msgid "NFS error %1, %2"
msgstr ""

#: nfsv2.cpp:392 nfsv2.cpp:443 nfsv3.cpp:430 nfsv3.cpp:574
#, kde-format
msgid "Unknown target"
msgstr ""

#~ msgid "No space left on device"
#~ msgstr "Құрылғыда орын қалған жоқ"

#~ msgid "Read only file system"
#~ msgstr "Тек оқитын файл жүйесі"

#~ msgid "An RPC error occurred."
#~ msgstr "RPC қатесі пайда болды."
