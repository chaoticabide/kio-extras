# translation of kio_nfs.po to
# Copyright (C) 2001,2002, 2006, 2007 Free Software Foundation, Inc.
# Majid Moggamel <Anatolia@linux-team.org>, 2001.
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002.
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
# Youssef Chahibi <chahibi@gmail.com>, 2007.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 02:01+0000\n"
"PO-Revision-Date: 2022-02-13 21:52+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: Arabic <kde-l10n-ar@kde.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.07.70\n"

#: kio_nfs.cpp:153
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr "لا يمكن العثور على إصدار NFS يدعمه '%1'"

#: kio_nfs.cpp:323
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr "ميفاق NFS يحتاج إلى اسم مضيف الخادم."

#: kio_nfs.cpp:353
#, kde-format
msgid "Failed to initialise protocol"
msgstr "فشل في بدء الميفاق"

#: kio_nfs.cpp:821
#, kde-format
msgid "RPC error %1, %2"
msgstr "خطأ RPC ‏ %1، %2"

#: kio_nfs.cpp:870
#, kde-format
msgid "Filename too long"
msgstr "اسم الملف طويل جداً"

#: kio_nfs.cpp:877
#, kde-format
msgid "Disk quota exceeded"
msgstr "تجاوز الحصة المخصصة على القرص"

#: kio_nfs.cpp:883
#, kde-format
msgid "NFS error %1, %2"
msgstr "خطأ NFS ‏%1، %2"

#: nfsv2.cpp:392 nfsv2.cpp:443 nfsv3.cpp:430 nfsv3.cpp:574
#, kde-format
msgid "Unknown target"
msgstr "مقصد مجهول"

#~ msgid "No space left on device"
#~ msgstr "لا توجد مساحة متبقية على الجهاز"

#~ msgid "Read only file system"
#~ msgstr "نظام الملفات صالح للقراءة فقط"

#~ msgid "An RPC error occurred."
#~ msgstr "حدث خطأ في RPC."
