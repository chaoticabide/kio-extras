# translation of kio_fish.po to Gujarati
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sweta Kothari <swkothar@redhat.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:37+0000\n"
"PO-Revision-Date: 2008-07-08 13:40+0530\n"
"Last-Translator: Sweta Kothari <swkothar@redhat.com>\n"
"Language-Team: Gujarati\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: fish.cpp:321
#, kde-format
msgid "Connecting..."
msgstr "જોડાઇ રહ્યા છે..."

#: fish.cpp:647
#, kde-format
msgid "Initiating protocol..."
msgstr "પ્રોટોકોલ શરૂઆત કરી રહ્યા છે..."

#: fish.cpp:684
#, kde-format
msgid "Local Login"
msgstr "સ્થાનીય પ્રવેશ"

#: fish.cpp:686
#, fuzzy, kde-format
#| msgid "SSH Authorization"
msgid "SSH Authentication"
msgstr "SSH સત્તાધિકરણ"

#: fish.cpp:723 fish.cpp:738
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:723 fish.cpp:738
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:821
#, kde-format
msgid "Disconnected."
msgstr "જોડાણ તોડી નાખેલ છે."
