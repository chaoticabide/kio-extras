# translation of kio_sftp.po to Gujarati
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sweta Kothari <swkothar@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_sftp\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-05 00:37+0000\n"
"PO-Revision-Date: 2009-01-07 15:31+0530\n"
"Last-Translator: Sweta Kothari <swkothar@redhat.com>\n"
"Language-Team: Gujarati\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: kio_sftp.cpp:262
#, fuzzy, kde-format
#| msgid "Incorrect username or password"
msgid "Incorrect or invalid passphrase"
msgstr "અયોગ્ય વપરાશકર્તા નામ અથવા પાસવર્ડ"

#: kio_sftp.cpp:311
#, kde-format
msgid "Could not allocate callbacks"
msgstr ""

#: kio_sftp.cpp:324
#, kde-format
msgid "Could not set log verbosity."
msgstr ""

#: kio_sftp.cpp:329
#, fuzzy, kde-format
#| msgid "Could not read SFTP packet"
msgid "Could not set log userdata."
msgstr "SFTP પેકેટ ને વાંચી શકાતુ નથી"

#: kio_sftp.cpp:334
#, fuzzy, kde-format
#| msgid "Could not read SFTP packet"
msgid "Could not set log callback."
msgstr "SFTP પેકેટ ને વાંચી શકાતુ નથી"

#: kio_sftp.cpp:370 kio_sftp.cpp:372 kio_sftp.cpp:883
#, kde-format
msgid "SFTP Login"
msgstr "SFTP પ્રવેશ"

#: kio_sftp.cpp:387
#, kde-format
msgid "Use the username input field to answer this question."
msgstr ""

#: kio_sftp.cpp:400
#, fuzzy, kde-format
#| msgid "Please enter your username and password."
msgid "Please enter your password."
msgstr "મહેરબાની કરીને તમારા વપરાશકર્તા નામ અને પાસવર્ડ ને દાખલ કરો."

#: kio_sftp.cpp:405 kio_sftp.cpp:886
#, fuzzy, kde-format
#| msgid "site:"
msgid "Site:"
msgstr "સાઇટ:"

#: kio_sftp.cpp:450
#, fuzzy, kde-format
#| msgid "Could not read SFTP packet"
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr "SFTP પેકેટ ને વાંચી શકાતુ નથી"

#: kio_sftp.cpp:570
#, kde-format
msgid "Could not create a new SSH session."
msgstr ""

#: kio_sftp.cpp:581 kio_sftp.cpp:585
#, fuzzy, kde-format
#| msgid "Could not read SFTP packet"
msgid "Could not set a timeout."
msgstr "SFTP પેકેટ ને વાંચી શકાતુ નથી"

#: kio_sftp.cpp:592
#, fuzzy, kde-format
#| msgid "Could not read SFTP packet"
msgid "Could not disable Nagle's Algorithm."
msgstr "SFTP પેકેટ ને વાંચી શકાતુ નથી"

#: kio_sftp.cpp:598 kio_sftp.cpp:603
#, kde-format
msgid "Could not set compression."
msgstr ""

#: kio_sftp.cpp:609
#, kde-format
msgid "Could not set host."
msgstr ""

#: kio_sftp.cpp:615
#, fuzzy, kde-format
#| msgid "Could not read SFTP packet"
msgid "Could not set port."
msgstr "SFTP પેકેટ ને વાંચી શકાતુ નથી"

#: kio_sftp.cpp:623
#, kde-format
msgid "Could not set username."
msgstr ""

#: kio_sftp.cpp:630
#, kde-format
msgid "Could not parse the config file."
msgstr ""

#: kio_sftp.cpp:645
#, fuzzy, kde-kuit-format
#| msgid "Opening SFTP connection to host %1:%2"
msgid "Opening SFTP connection to host %1:%2"
msgstr "યજમાન %1 માં SFTP જોડાણ ને ખોલી રહ્યા છે:%2"

#: kio_sftp.cpp:685
#, kde-format
msgid "Could not get server public key type name"
msgstr ""

#: kio_sftp.cpp:697
#, kde-format
msgid "Could not create hash from server public key"
msgstr ""

#: kio_sftp.cpp:706
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr ""

#: kio_sftp.cpp:765
#, kde-format
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""

#: kio_sftp.cpp:782
#, fuzzy, kde-format
#| msgid "Warning: Host's identity changed."
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr "ચેતવણી: યજમાન નાં ઓળખાણને બદલેલ છે."

#: kio_sftp.cpp:784
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""

#: kio_sftp.cpp:794
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr ""

#: kio_sftp.cpp:796
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""

#: kio_sftp.cpp:805
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr ""

#: kio_sftp.cpp:828 kio_sftp.cpp:847 kio_sftp.cpp:862 kio_sftp.cpp:875
#: kio_sftp.cpp:927 kio_sftp.cpp:937
#, kde-format
msgid "Authentication failed."
msgstr "સત્તાધિકરણ નિષ્ફળ."

#: kio_sftp.cpp:835
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""

#: kio_sftp.cpp:884
#, kde-format
msgid "Please enter your username and password."
msgstr "મહેરબાની કરીને તમારા વપરાશકર્તા નામ અને પાસવર્ડ ને દાખલ કરો."

#: kio_sftp.cpp:895
#, kde-format
msgid "Incorrect username or password"
msgstr "અયોગ્ય વપરાશકર્તા નામ અથવા પાસવર્ડ"

#: kio_sftp.cpp:944
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""

#: kio_sftp.cpp:949
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr ""

#: kio_sftp.cpp:953
#, kde-format
msgid "Successfully connected to %1"
msgstr "%1 માં સફળતાપૂર્વક જોડાયેલ છે"

#: kio_sftp.cpp:1006
#, kde-format
msgid "Invalid sftp context"
msgstr ""

#: kio_sftp.cpp:1538
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""

#~ msgid "Warning: Cannot verify host's identity."
#~ msgstr "ચેતવણી: યજમાનનાં ઓળખાણ ને ચકાસી શકાતુ નથી."

#, fuzzy
#~| msgid "No hostname specified"
#~ msgid "No hostname specified."
#~ msgstr "યજમાન સ્પષ્ટ થયેલ નથી"

#~ msgid "An internal error occurred. Please retry the request again."
#~ msgstr "આંતરિક ભૂલ ઉત્પન્ન થયેલ છે. મહેરબાની કરીને સૂચનાનો ફરીથી પ્રયત્ન કરો."

#~ msgid "Please enter your username and key passphrase."
#~ msgstr "મહેરબાની કરીને તમારા વપરાશકર્તા નામ અને કી પાસફ્રેઝ ને દાખલ કરો."

#~ msgid "Connection failed."
#~ msgstr "જોડાણ નિષ્ફળ."

#~ msgid "Connection closed by remote host."
#~ msgstr "દૂરસ્થ યજમાન દ્દારા બંધ થયેલ જોડાણ."

#, fuzzy
#~| msgid "Unexpected SFTP error: %1"
#~ msgid "unexpected SFTP error: %1"
#~ msgstr "અનિચ્છનિય SFTP ભૂલ: %1"

#~ msgid "SFTP version %1"
#~ msgstr "SFTP આવૃત્તિ %1"

#~ msgid "Protocol error."
#~ msgstr "પ્રોટોકોલ ભૂલ."

#~ msgid "An internal error occurred. Please try again."
#~ msgstr "આંતરિક ભૂલ ઉત્પન્ન થયેલ છે. મહેરબાની કરીને ફરીથી પ્રયત્ન કરો."

#~ msgid ""
#~ "Unknown error was encountered while copying the file to '%1'. Please try "
#~ "again."
#~ msgstr ""
#~ "અજ્ઞાત ભૂલ શોધાયેલ હતી જ્યારે '%1' માં ફાઇલની નકલ કરી રહ્યા હોય. મહેરબાની કરીને "
#~ "ફરીથી પ્રયત્ન કરો."

#~ msgid "The remote host does not support renaming files."
#~ msgstr "દૂરસ્થ યજમાન પુન:નામ ની ફાઇલોને આધાર આપતુ નથી."

#~ msgid "The remote host does not support creating symbolic links."
#~ msgstr "દૂરસ્થ યજમાન સંકેત કડીઓને બનાવવા નો આધાર આપતુ નથી."

#~ msgid "Connection closed"
#~ msgstr "જોડાણ બંધ થયેલ છે"

#~ msgid "End of file."
#~ msgstr "ફાઇલ નો અંત."

#~ msgid "SFTP command failed for an unknown reason."
#~ msgstr "અજ્ઞાત કારણ માટે SFTP આદેશ નિષ્ફળ."

#~ msgid "The SFTP server received a bad message."
#~ msgstr "SFTP સર્વર ને ખરાબ સંદેશ ને મેળવેલ છે."

#~ msgid "You attempted an operation unsupported by the SFTP server."
#~ msgstr "SFTP સર્વર દ્દારા બિનઆધારિત થયેલ ક્રિયાનો પ્રયત્ન થયેલ છે."

#~ msgid "Error code: %1"
#~ msgstr "ભૂલ કોડ: %1"

#~ msgid "Cannot specify a subsystem and command at the same time."
#~ msgstr "એક જ સમયે આદેશ અને ઉપસિસ્ટમ ને સ્પષ્ટ કરી શકાતુ નથી."

#~ msgid "No options provided for ssh execution."
#~ msgstr "ssh ને ચલાવવા માટે વિકલ્પો પૂરા પાડેલ નથી."

#~ msgid "Failed to execute ssh process."
#~ msgstr "ssh પ્રક્રિયા ને ચલાવવા માટે નિષ્ફળ."

#~ msgid "Error encountered while talking to ssh."
#~ msgstr "ssh ની વાત કરી રહ્યા હોય ત્યારે ભૂલ શોધાયેલ છે."

#~ msgid "Please supply a password."
#~ msgstr "મહેરબાની કરીને પાસવર્ડ ને પૂરો પાડો."

#~ msgid "Please supply the passphrase for your SSH private key."
#~ msgstr "મહેરબાની કરીને તમારી SSH ખાનગી કી માટે પાસફ્રેઝ ને પૂરો પાડો."

#~ msgid "Authentication to %1 failed"
#~ msgstr "%1 માં સત્તાધિકરણ નિષ્ફળ"

#~ msgid ""
#~ "The identity of the remote host '%1' could not be verified because the "
#~ "host's key is not in the \"known hosts\" file."
#~ msgstr ""
#~ "દૂરસ્થ યજમાન '%1' ની ઓળખાણ ને ચકાસી શકાતી નથી કારણ કે યજમાનની કી \"જાણીતા "
#~ "યજમાનો\" ફાઇલ માં નથી."

#~ msgid ""
#~ " Manually, add the host's key to the \"known hosts\" file or contact your "
#~ "administrator."
#~ msgstr ""
#~ " જાતે જ, \"જાણીતા યજમાનો\" ફાઇલ માં યજમાનની કી ને ઉમેરો અથવા તમારા વહીવટકર્તાને "
#~ "સંપર્ક કરો."

#~ msgid " Manually, add the host's key to %1 or contact your administrator."
#~ msgstr " જાતે જ, %1 માં યજમાનની કી ને ઉમેરો અથવા તમારા વહીવટકર્તાનો સંપર્ક કરો."

#~ msgid "Host key was rejected."
#~ msgstr "યજમાન કી ને રદ કરેલ હતી."

#~ msgid "Please enter a username and password"
#~ msgstr "મહેરબાની કરીને વપરાશકર્તા નામ અને પાસવર્ડ ને દાખલ કરો"
